/*define(['ko'], function(ko) {
    'use strict';

    return function(config) {
        var viewModel = {
            title: ko.observable('Foo'),
            description: ko.observable('Some description'),
            counter: ko.observable(0)
        };

        viewModel.title.subscribe(
            function(val) {
                console.log('Observable was updated! ', val);
            }
        );

        viewModel.summary = ko.computed(function() {
            return "Title: " + viewModel.title() + ", " + "Description: " + viewModel.description();
        });
        return viewModel;
    };
});*/

define(['uiComponent'], function(Component) {
    'use strict';

    return Component.extend({
        defaults: {
            title: "This is a UI component",
            message: "Hello from component 1",
            tracks: {
                counter: true,
                message: true
            },
            exports: {
                message: "my-component.my-child:message"
            },
            template: "StudioModerna_UiExample/counter"
        },
        counter: 0,
        count: function() {
            this.counter++;
        }
    });
});