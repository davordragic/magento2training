<?php

namespace StudioModerna\ExampleObserver\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class ExampleCustomerLoginObserver implements ObserverInterface
{
    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        echo __METHOD__ . '()';
    }
}