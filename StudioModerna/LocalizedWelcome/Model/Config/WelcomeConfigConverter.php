<?php

namespace StudioModerna\LocalizedWelcome\Model\Config;

use Magento\Framework\Config\ConverterInterface;

class WelcomeConfigConverter implements ConverterInterface
{
    /**
     * Convert config
     *
     * @param \DOMDocument $source
     * @return array
     */
    public function convert($source)
    {
        $root = $this->childElements($source)[0];
        return $this->buildCountryWelcomeMap($this->childElements($root));
    }

    /**
     * @param \DOMNode $node
     * @return \DOMElement[]
     */
    private function childElements(\DOMNode $node): array {
        return array_filter(iterator_to_array($node->childNodes), function(\DOMNode $child) {
            return $child->nodeType === XML_ELEMENT_NODE;
        });
    }

    /**
     * @param \DOMElement[] $countryNodes
     * @return array[]
     */
    private function buildCountryWelcomeMap(array $countryNodes)
    {
        $countryMap = [];
        foreach ($countryNodes as $countryNode) {
            $countryCode = strtoupper($countryNode->getAttribute('code'));
            $stateNodes = $this->childElements($countryNode);
            $countryMap[$countryCode] = $this->buildStateWelcomeMap($stateNodes);
        }
        return $countryMap;
    }

    /**
     * @param \DOMElement[] $stateNodes
     * @return string[]
     */
    private function buildStateWelcomeMap($stateNodes)
    {
        $map = [];
        foreach ($stateNodes as $stateNode) {
            $stateCode = strtoupper($stateNode->getAttribute('code'));
            $map[$stateCode] = $stateNode->textContent;
        }
        return $map;
    }
}