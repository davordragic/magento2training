<?php

namespace StudioModerna\LocalizedWelcome\Model;

use Magento\Framework\Config\DataInterface;

class WelcomeConfig
{
    /**
     * @var DataInterface
     */
    private $config;

    public function __construct(DataInterface $config)
    {
        $this->config = $config;
    }

    /**
     * @param string $country
     * @param string $state
     * @return string|null
     */
    public function getLocalizedWelcome($country, $state)
    {
        if ('' === $state) {
            return $this->getDefaultForCountry($country);
        }
        $path = strtoupper($country . '/' . $state);
        $localizedWelcome = $this->config->get($path);
        return $localizedWelcome ?? $this->getDefaultForCountry($country);
    }

    private function getDefaultForCountry(string $country)
    {
        $path = strtoupper($country . '/default');
        return $this->config->get($path);
    }
}