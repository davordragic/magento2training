<?php

namespace StudioModerna\LocalizedWelcome\Plugin;

use Magento\Theme\Block\Html\Header;
use StudioModerna\GeoIp\Model\VisitorLocation;
use StudioModerna\LocalizedWelcome\Model\WelcomeConfig;

class LocalizedWelcomePlugin
{
    /**
     * @var WelcomeConfig
     */
    private $welcomeConfig;
    /**
     * @var VisitorLocation
     */
    private $visitorLocation;

    public function __construct(WelcomeConfig $welcomeConfig, VisitorLocation $visitorLocation)
    {
        $this->welcomeConfig = $welcomeConfig;
        $this->visitorLocation = $visitorLocation;
    }

    public function afterGetWelcome(Header $subject, $result)
    {
        $country = $this->visitorLocation->getCountry();
        $state = $this->visitorLocation->getState();
        $localizedWelcome = $this->welcomeConfig->getLocalizedWelcome($country, $state);
        return $localizedWelcome ?? $result;
    }

}