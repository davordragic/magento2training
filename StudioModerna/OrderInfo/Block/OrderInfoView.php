<?php

namespace StudioModerna\OrderInfo\Block;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\Escaper;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use StudioModerna\OrderInfo\Model\OrderInfo;

class OrderInfoView implements ArgumentInterface
{

    /**
     * @var OrderInfo
     */
    private $orderInfo;
    /**
     * @var RequestInterface
     */
    private $request;
    /**
     * @var Escaper
     */
    private $escaper;
    /**
     * @var PriceCurrencyInterface
     */
    private $priceCurrency;

    public function __construct(
        OrderInfo $orderInfo,
        RequestInterface $request,
        Escaper $escaper,
        PriceCurrencyInterface $priceCurrency
    )
    {
        $this->orderInfo = $orderInfo;
        $this->request = $request;
        $this->escaper = $escaper;
        $this->priceCurrency = $priceCurrency;
    }

    private function getOrderInfo(): array
    {
        return $this->orderInfo->asArray($this->request->getParam('id'));
    }

    public function getOrderId()
    {
        return $this->escaper->escapeHtml($this->getOrderInfo()["id"]);
    }

    public function getOrderStatus()
    {
        return $this->escaper->escapeHtml($this->getOrderInfo()["status"]);
    }

    public function getOrderTotal()
    {
        return $this->priceCurrency->format($this->escaper->escapeHtml($this->getOrderInfo()["total"]), false);
    }
}