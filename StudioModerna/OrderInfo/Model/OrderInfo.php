<?php
/**
 * Created by PhpStorm.
 * User: marko.ambrozic
 * Date: 6. 12. 2017
 * Time: 09:54
 */

namespace StudioModerna\OrderInfo\Model;


use Magento\Sales\Api\Data\OrderItemInterface;
use Magento\Sales\Api\OrderRepositoryInterface;

class OrderInfo
{
    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    public function __construct(OrderRepositoryInterface $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function asArray($orderId)
    {
        $order = $this->orderRepository->get($orderId);
        return [
            'id' => $order->getEntityId(),
            'total' => $order->getGrandTotal(),
            'status' => $order->getStatus(),
            'items'  => $this->buildOrderItemInfo($order->getItems())
        ];
    }

    private function buildOrderItemInfo(array $items)
    {
        return array_values(array_map(function(OrderItemInterface $item) {
            return [
                'id' => $item->getItemId(),
                'sku' => $item->getSku(),
                'qty' => $item->getQtyOrdered()
            ];
        }, $items));
    }
}