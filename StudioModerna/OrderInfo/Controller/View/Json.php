<?php

namespace StudioModerna\OrderInfo\Controller\View;

use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use StudioModerna\OrderInfo\Model\OrderInfo;

class Json implements ActionInterface
{
    /**
     * @var RequestInterface
     */
    private $request;
    /**
     * @var JsonFactory
     */
    private $jsonResultFactory;
    /**
     * @var OrderInfo
     */
    private $orderInfo;

    public function __construct(RequestInterface $request, JsonFactory $jsonResultFactory, OrderInfo $orderInfo)
    {
        $this->request = $request;
        $this->jsonResultFactory = $jsonResultFactory;
        $this->orderInfo = $orderInfo;
    }

    public function execute()
    {
        $id = $this->request->getParam('id');
        $jsonResult = $this->jsonResultFactory->create();
        $jsonResult->setData($this->orderInfo->asArray($id));

        return $jsonResult;
    }
}