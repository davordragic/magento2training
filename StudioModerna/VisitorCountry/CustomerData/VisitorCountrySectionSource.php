<?php

namespace StudioModerna\VisitorCountry\CustomerData;

use Magento\Customer\CustomerData\SectionSourceInterface;
use StudioModerna\GeoIp\Model\VisitorLocation;

class VisitorCountrySectionSource implements SectionSourceInterface
{
    /**
     * @var VisitorLocation
     */
    private $visitorLocation;

    public function __construct(VisitorLocation $visitorLocation)
    {
        $this->visitorLocation = $visitorLocation;
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getSectionData()
    {
        return [
            'country' => $this->visitorLocation->getCountry() . ' ' . time(),
        ];
    }
}