/*define(["jquery"], function($) {
    'use strict';

    return function(config){
        var viewModel = {
            country: "SI",
            hasCountry: true
        };
        viewModel.hasCountry = viewModel.country.length > 0;

        $.ajax(

        );

        return viewModel;
    };
});*/

define(['uiComponent', 'Magento_Customer/js/customer-data', 'ko'], function(Component, customerData, ko) {
    'use strict';

    var country = customerData.get('visitor_country');

    return Component.extend({
        country: ko.computed(function() {
            return country().country;
        }),
        defaults: {
            tracks: {
                country: true
            }
        },
        hasCountry: function() {
            return !!this.country();
        }
    });
});