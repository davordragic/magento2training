<?php

namespace StudioModerna\GeoIp\Plugin;

use Psr\Log\LoggerInterface;
use StudioModerna\GeoIp\Model\GeoIpLookup\NekudoGeoIpLookup;

class LookupLoggingPlugin
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * LookupLoggingPlugin constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function beforeLookupIp(NekudoGeoIpLookup $subject, $ip)
    {
        $this->logger->debug(sprintf('before %s::lookupIp(%s)', get_class($subject), $ip));
        return [$ip];
    }

    public function aroundLookupIp(NekudoGeoIpLookup $subject, callable $proceed, $ip)
    {
        return $proceed($ip);
    }

    public function afterLookupIp(NekudoGeoIpLookup $subject, $result, $ip)
    {
        return $result;
    }
}