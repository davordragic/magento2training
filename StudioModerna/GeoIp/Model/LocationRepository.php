<?php declare(strict_types=1);

namespace StudioModerna\GeoIp\Model;

use StudioModerna\GeoIp\Api\LocationRepositoryInterface;
use StudioModerna\GeoIp\Model\GeoIpLookup\GeoIpLookupInterface;

class LocationRepository implements LocationRepositoryInterface
{
    /**
     * @var GeoIpLookupInterface
     */
    private $geoIpLookup;
    /**
     * @var LocationFactory
     */
    private $locationFactory;
    /**
     * @var VisitorLocationFactory
     */
    private $visitorLocationFactory;

    /**
     * LocationRepository constructor.
     * @param GeoIpLookupInterface $geoIpLookup
     * @param LocationFactory $locationFactory
     */
    public function __construct(
        GeoIpLookupInterface $geoIpLookup,
        LocationFactory $locationFactory,
        VisitorLocationFactory $visitorLocationFactory
    )
    {
        $this->geoIpLookup = $geoIpLookup;
        $this->locationFactory = $locationFactory;
        $this->visitorLocationFactory = $visitorLocationFactory;
    }

    /**
     * @param string $ip
     * @return Location
     */
    public function getByIp($ip)
    {
        $locationData = $this->geoIpLookup->lookupIp($ip);
        return $this->locationFactory->create([
            'country' => $locationData['country'],
            'state' => $locationData['state']
        ]);
    }

    public function getVisitorLocation()
    {
        return $this->visitorLocationFactory->create();
    }
}