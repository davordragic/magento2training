<?php

namespace StudioModerna\GeoIp\Model\GeoIpLookup;

use Magento\Framework\App\CacheInterface;

class CachingGeoIpLookupDecorator implements GeoIpLookupInterface
{

    /**
     * @var CacheInterface
     */
    private $cache;
    /**
     * @var GeoIpLookupInterface
     */
    private $delegate;

    public function __construct(CacheInterface $cache, GeoIpLookupInterface $delegate)
    {
        $this->cache = $cache;
        $this->delegate = $delegate;
    }

    /**
     * @param string $ip
     * @return string[]
     */
    public function lookupIp($ip)
    {
        $cacheId = 'studiomoderna_geoip_' . $ip;
        $cachedLocationData = $this->cache->load($cacheId);
        if ($cachedLocationData) {
            return json_decode($cachedLocationData, true);
        }

        $lookupResult = $this->delegate->lookupIp($ip);
        $this->cache->save(json_encode($lookupResult), $cacheId);
        return $lookupResult;
    }
}