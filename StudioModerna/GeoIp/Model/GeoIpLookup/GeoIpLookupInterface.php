<?php declare(strict_types=1);

namespace StudioModerna\GeoIp\Model\GeoIpLookup;

interface GeoIpLookupInterface
{
    /**
     * @param string $ip
     * @return string[]
     */
    public function lookupIp($ip);
}