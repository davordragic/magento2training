<?php declare(strict_types=1);

namespace StudioModerna\GeoIp\Model\GeoIpLookup;


class NekudoGeoIpLookup implements GeoIpLookupInterface
{

    /**
     * @param string $ip
     * @return string[]
     */
    public function lookupIp($ip)
    {
        $url = sprintf('http://geoip.nekudo.com/api/%s/full', $ip);
        $responseBody = file_get_contents($url);
        $responseBody = json_decode($responseBody, true);

        return [
            'country' => $responseBody['country']['iso_code'] ?? '',
            'state' => $responseBody['subdivisions'][0]['iso_code'] ?? ''
        ];
    }
}