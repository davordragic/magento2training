<?php

namespace StudioModerna\GeoIp\Model;

use StudioModerna\GeoIp\Api\Data\LocationInterface;

class Location implements LocationInterface
{
    /**
     * @var string
     */
    private $country;

    /**
     * @var string
     */
    private $state;

    public function __construct($country, $state)
    {
        $this->country = $country;
        $this->state = $state;
    }

    public function getCountry() 
    {
        return $this->country;
    }
    
    public function getState()
    {
        return $this->state;
    }
}