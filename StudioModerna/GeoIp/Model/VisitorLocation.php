<?php declare(strict_types=1);

namespace StudioModerna\GeoIp\Model;

use Magento\Framework\HTTP\PhpEnvironment\RemoteAddress;
use StudioModerna\GeoIp\Api\Data\LocationInterface;

/**
 * @api
 */
class VisitorLocation implements LocationInterface
{
    /**
     * @var RemoteAddress
     */
    private $remoteAddress;
    /**
     * @var LocationRepository
     */
    private $locationRepository;

    public function __construct(RemoteAddress $remoteAddress, LocationRepository $locationRepository)
    {
        $this->remoteAddress = $remoteAddress;
        $this->locationRepository = $locationRepository;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->getLocation()->getCountry();
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->getLocation()->getState();
    }

    private function getVisitorIp(): string
    {
        return (string) $this->remoteAddress->getRemoteAddress();
    }

    private function getLocation(): Location
    {
        $ip = $this->getVisitorIp();
        $ip = '1.2.3.4';
        return $this->locationRepository->getByIp($ip);
    }
}