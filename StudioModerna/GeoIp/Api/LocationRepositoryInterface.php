<?php
namespace StudioModerna\GeoIp\Api;

interface LocationRepositoryInterface
{
    /**
     * @param string $ip
     * @return \StudioModerna\GeoIp\Api\Data\LocationInterface
     */
    public function getByIp($ip);

    /**
     * @return \StudioModerna\GeoIp\Api\Data\LocationInterface
     */
    public function getVisitorLocation();
}