<?php

namespace StudioModerna\GeoIp\Api\Data;

interface LocationInterface
{
    /**
     * @return string
     */
    public function getCountry();

    /**
     * @return string
     */
    public function getState();
}