<?php
namespace StudioModerna\Content\Controller\Adminhtml\Article;

class Index extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = 'StudioModerna_Content::articles';  
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('*/index/index');
        return $resultRedirect;
    }     
}
