<?php

namespace StudioModerna\Content\Controller\Adminhtml\Articles;


use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\Page as BackendPage;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\View\Result\PageFactory;

class Index extends Action
{
    const ADMIN_RESOURCE = 'StudioModerna_Content::articles';

    /**
     * @var PageFactory
     */
    private $pageFactory;

    public function __construct(Action\Context $context, PageFactory $pageFactory)
    {
        parent::__construct($context);
        $this->pageFactory = $pageFactory;
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        /** @var BackendPage $page */
        $page = $this->pageFactory->create();

        $page->setActiveMenu('StudioModerna_Content::article');
        $page->getConfig()->getTitle()->prepend('Articles');

        return $page;
    }
}