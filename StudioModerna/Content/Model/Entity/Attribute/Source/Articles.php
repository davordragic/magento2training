<?php

namespace StudioModerna\Content\Model\Entity\Attribute\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use StudioModerna\Content\Model\ResourceModel\Article\CollectionFactory;

class Articles extends AbstractSource
{
    /**
     * @var CollectionFactory
     */
    private $articleFactory;
    private $options;

    public function __construct(CollectionFactory $articleFactory)
    {
        $this->articleFactory = $articleFactory;
    }

    /**
     * Retrieve All options
     *
     * @return array
     */
    public function getAllOptions()
    {
        if (!$this->options) {
            $this->options = $this->buildOptionsArray();
        }
        return $this->options;
    }

    private function buildOptionsArray()
    {
        $articles = $this->articleFactory->create();
        return array_merge(
            [['value' => '', 'label' => 'None']],
            $articles->toOptionArray()
        );
    }
}