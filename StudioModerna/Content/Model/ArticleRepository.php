<?php

namespace StudioModerna\Content\Model;

use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use StudioModerna\Content\Api\ArticleRepositoryInterface;
use StudioModerna\Content\Api\Data\ArticleInterface;
use StudioModerna\Content\Api\Data\ArticleInterfaceFactory;
use StudioModerna\Content\Api\Data\ArticleSearchResultsInterfaceFactory;
use StudioModerna\Content\Model\ResourceModel\Article as ArticleResource;
use StudioModerna\Content\Model\ResourceModel\Article\CollectionFactory;


class ArticleRepository implements ArticleRepositoryInterface
{
    /**
     * @var ArticleResource
     */
    private $articleResource;
    /**
     * @var ArticleFactory
     */
    private $articleFactory;
    /**
     * @var ArticleInterfaceFactory
     */
    private $dtoFactory;
    /**
     * @var DataObjectHelper
     */
    private $dataObjectHelper;
    /**
     * @var ArticleResource\CollectionFactory
     */
    private $articleCollectionFactory;
    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;
    /**
     * @var ArticleSearchResultsInterfaceFactory
     */
    private $searchResultsFactory;

    public function __construct(
        ArticleResource $articleResource,
        ArticleFactory $articleFactory,
        ArticleInterfaceFactory $dtoFactory,
        DataObjectHelper $dataObjectHelper,
        CollectionFactory $articleCollectionFactory,
        CollectionProcessorInterface $collectionProcessor,
        ArticleSearchResultsInterfaceFactory $searchResultsFactory
    )
    {
        $this->articleResource = $articleResource;
        $this->articleFactory = $articleFactory;
        $this->dtoFactory = $dtoFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->articleCollectionFactory = $articleCollectionFactory;
        $this->collectionProcessor = $collectionProcessor;
        $this->searchResultsFactory = $searchResultsFactory;
    }

    public function get($id)
    {
        $article = $this->articleFactory->create();
        $this->articleResource->load($article, $id);
        if (!$article->getId()) {
            throw new NoSuchEntityException('No article with ID %1 found', $id);
        }

        return $this->modelToDto($article);
    }

    private function modelToDto(Article $article) {
        $dto = $this->dtoFactory->create();
        $this->dataObjectHelper->populateWithArray($dto, $article->getData(), ArticleInterface::class);
        return $dto;
    }

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \StudioModerna\Content\Api\Data\ArticleSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $collection = $this->articleCollectionFactory->create();
        $this->collectionProcessor->process($searchCriteria, $collection);
        $searchResult = $this->searchResultsFactory->create();

        $dtos = [];
        /** @var Article $article */
        foreach ($collection->getItems() as $article) {
            $dtos[] = $this->modelToDto($article);
        }

        $searchResult->setItems($dtos);
        $searchResult->setSearchCriteria($searchCriteria);
        $searchResult->setTotalCount($collection->getSize());

        return $searchResult;
    }
}