<?php
/**
 * Created by PhpStorm.
 * User: marko.ambrozic
 * Date: 7. 12. 2017
 * Time: 13:11
 */

namespace StudioModerna\Content\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Article extends AbstractDb
{
    const TABLE = 'studiomoderna_content_article';
    const ID_FIELD = 'id';

    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(self::TABLE, self::ID_FIELD);
    }
}