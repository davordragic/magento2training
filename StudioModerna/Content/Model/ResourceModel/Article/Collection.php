<?php

namespace StudioModerna\Content\Model\ResourceModel\Article;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use StudioModerna\Content\Model\Article as Model;
use StudioModerna\Content\Model\ResourceModel\Article as ResourceModel;

class Collection extends AbstractCollection
{
    public function _construct()
    {
        $this->_init(Model::class, ResourceModel::class);
    }

    public function getIdFieldName()
    {
        return $this->getResource()->getIdFieldName();
    }

    public function toOptionArray()
    {
        return $this->_toOptionArray(
            ResourceModel::ID_FIELD,
            Model::TITLE
        );
    }


}