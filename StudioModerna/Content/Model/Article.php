<?php

namespace StudioModerna\Content\Model;

use Magento\Framework\Model\AbstractModel;
use StudioModerna\Content\Api\Data\ArticleInterface;

class Article extends AbstractModel implements ArticleInterface
{
    public function _construct()
    {
        $this->_init(ResourceModel\Article::class);
    }

    public function setTitle($title)
    {
        $this->setData(self::TITLE, $title);
    }

    public function getTitle()
    {
        return $this->_getData(self::TITLE);
    }

    public function setContent($content)
    {
        $this->setData(self::CONTENT, $content);
    }

    public function getContent() {
        return $this->_getData(self::CONTENT);
    }

    public function isEnabled()
    {
        return $this->_getData(self::IS_ENABLED);
    }

    public function setIsEnabled($isEnabled)
    {
        $this->setData(self::IS_ENABLED, $isEnabled);
    }

    public function getFrom() {
        $this->_getData(self::FROM);
    }

    public function setFrom($from) {
        $this->setData(self::FROM, $from);
    }

    public function getTo() {
        $this->_getData(self::TO);
    }

    public function setTo($to) {
        $this->setData(self::TO, $to);
    }
}