<?php

namespace StudioModerna\Content\Model;

use Magento\Framework\Api\SearchResults;
use StudioModerna\Content\Api\Data\ArticleSearchResultsInterface;

class ArticleSearchResults extends SearchResults implements ArticleSearchResultsInterface
{

}