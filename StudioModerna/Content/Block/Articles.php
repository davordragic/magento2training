<?php

namespace StudioModerna\Content\Block;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use StudioModerna\Content\Api\ArticleRepositoryInterface;
use StudioModerna\Content\Api\Data\ArticleInterface;

class Articles implements ArgumentInterface
{
    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;
    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;
    /**
     * @var ArticleRepositoryInterface
     */
    private $articleRepository;

    public function __construct(
        ProductRepositoryInterface $productRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        ArticleRepositoryInterface $articleRepository
    )
    {
        $this->productRepository = $productRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->articleRepository = $articleRepository;
    }

    /**
     * @return ArticleInterface[]
     */
    public function getArticles()
    {
        $products = $this->getProductsWithArticles();
        $articleIds = $this->getArticleIdsFromProducts($products);

        $searchCriteria = $this->searchCriteriaBuilder->addFilter(ArticleInterface::ID, $articleIds, 'in');
        $result = $this->articleRepository->getList($searchCriteria->create());
        return $result->getItems();
    }

    private function getArticleIdsFromProducts($products) {
        $articleIds = [];
        /*foreach ($products as $product) {
            $articleIds = $product->getCustomAttribute('article_id')->getValue();
        }*/

        $articleIds = array_reduce($products, function($carry, $item) {
            $carry[] = $item->getCustomAttribute('article_id')->getValue();
            return $carry;
        }, $articleIds);
        return $articleIds;
    }

    /**
     * @return ProductInterface[]
     */
    private function getProductsWithArticles(): array
    {
        $searchCriteria = $this->searchCriteriaBuilder->addFilter('article_id', 0, 'gt');
        $result = $this->productRepository->getList($searchCriteria->create());
        return $result->getItems();
    }

}