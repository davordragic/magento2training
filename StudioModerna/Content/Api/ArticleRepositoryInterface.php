<?php

namespace StudioModerna\Content\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface ArticleRepositoryInterface
{
    /**
     * @param int $id
     * @return \StudioModerna\Content\Api\Data\ArticleInterface
     */
    public function get($id);

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \StudioModerna\Content\Api\Data\ArticleSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria);
}