<?php

namespace StudioModerna\Content\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface ArticleSearchResultsInterface extends SearchResultsInterface
{
    /**
     * @return \StudioModerna\Content\Api\Data\ArticleInterface[]
     */
    public function getItems();
}