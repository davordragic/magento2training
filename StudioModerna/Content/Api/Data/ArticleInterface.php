<?php

namespace StudioModerna\Content\Api\Data;


interface ArticleInterface
{
    const ID = 'id';
    const TITLE = 'title';
    const CONTENT = 'content';
    const IS_ENABLED = 'is_enabled';
    const FROM = 'from';
    const TO = 'to';

    /**
     * @param $title
     * @return void
     */
    public function setTitle($title);

    /**
     * @return string
     */
    public function getTitle();

    /**
     * @param $content
     * @return void
     */
    public function setContent($content);

    /**
     * @return string
     */
    public function getContent();

    /**
     * @param $isEnabled
     * @return void
     */
    public function setIsEnabled($isEnabled);

    /**
     * @return bool
     */
    public function isEnabled();

    /**
     * @param $from
     * @return void
     */
    public function setFrom($from);

    /**
     * @return string
     */
    public function getFrom();

    /**
     * @param $to
     * @return void
     */
    public function setTo($to);

    /**
     * @return string
     */
    public function getTo();

}