<?php

namespace StudioModerna\Content\Setup;

use Magento\Catalog\Api\Data\ProductAttributeInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;

class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * Upgrades data for a module
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        if (version_compare('0.0.3', $context->getVersion(), '>')) {
            $this->addProductArticleIdAttribute($setup);
        }
        if (version_compare('0.0.6', $context->getVersion(), '>')) {
            $this->setArticleIdAttributeSourceModel($setup);
        }
    }

    private function addProductArticleIdAttribute(ModuleDataSetupInterface $setup)
    {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $eavSetup->addAttribute(
            ProductAttributeInterface::ENTITY_TYPE_CODE,
            'article_id',
            [
                'type'             => 'int',
                'input'            => 'select',
                'label'            => 'Article',
                'source'           => \Magento\Eav\Model\Entity\Attribute\Source\Boolean::class,
                'is_required'      => 0,
                'user_defined'     => 1,
                'global'           => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                'visible_on_front' => 1,
                'group'            => 'Product Details',
                'sort_order'       => '50',
            ]
        );
    }

    private function setArticleIdAttributeSourceModel(ModuleDataSetupInterface $setup)
    {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $eavSetup->updateAttribute(
            ProductAttributeInterface::ENTITY_TYPE_CODE,
            'article_id',
            'source_model',
            \StudioModerna\Content\Model\Entity\Attribute\Source\Articles::class
        );
    }
}