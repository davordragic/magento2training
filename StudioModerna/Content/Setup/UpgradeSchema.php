<?php

namespace StudioModerna\Content\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use StudioModerna\Content\Model\ResourceModel\Article as ResourceModel;
use StudioModerna\Content\Model\Article;

class UpgradeSchema implements UpgradeSchemaInterface
{
    private $changes = [
        '0.0.2' => 'createArticleTable'
    ];
    /**
     * Upgrades DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        foreach ($this->changes as $version => $change) {
            if (version_compare($version, $context->getVersion(), '>')) {
                $this->{$change}($setup);
            }
        }
    }

    private function createArticleTable(SchemaSetupInterface $setup)
    {
        $table = $setup->getConnection()->newTable($setup->getTable(ResourceModel::TABLE));
        $table->addColumn(ResourceModel::ID_FIELD, $table::TYPE_INTEGER, null, [
            'primary' => true,
            'identity'    => true,
            'unsigned'    => true,
            'nullable'    => false
        ]);
        $table->addColumn(Article::TITLE, $table::TYPE_TEXT, '255', ['nullable' => false]);
        $table->addColumn(Article::CONTENT, $table::TYPE_TEXT, '10k', ['nullable' => false, 'default' => '']);
        $table->addColumn(Article::IS_ENABLED, $table::TYPE_BOOLEAN, null, [
            'nullable' => false,
            'default' => 0
        ]);
        $table->addColumn(Article::FROM, $table::TYPE_TIMESTAMP);
        $table->addColumn(Article::TO, $table::TYPE_TIMESTAMP);

        $setup->getConnection()->createTable($table);
    }
}