<?php

namespace StudioModerna\Content\Test\Integration\Model;

use Magento\TestFramework\ObjectManager;
use PHPUnit\Framework\TestCase;
use StudioModerna\Content\Model\Article;
use StudioModerna\Content\Model\ResourceModel\Article as ArticleResource;

/**
 * @magentoDbIsolation enabled
 */
class ArticleOrmModelTest extends TestCase
{
    public function testCanSaveAndLoad() {
        /** @var Article $article */
        $article = ObjectManager::getInstance()->create(Article::class);
        /** @var Article $loadedArticle */
        $loadedArticle = ObjectManager::getInstance()->create(Article::class);
        /** @var ArticleResource $resource */
        $resource = ObjectManager::getInstance()->create(ArticleResource::class);

        $article->setTitle("Foo");
        $article->setIsEnabled(1);
        $article->setContent("test");

        $resource->save($article);
        $resource->load($loadedArticle, $article->getId());

        $this->assertEquals($article->getId(), $loadedArticle->getId());
        $this->assertEquals($article->isEnabled(), $loadedArticle->isEnabled());
    }
}