<?php
namespace StudioModerna\Content\Ui\Component\Listing\Column\Articlegrid;

class PageActions extends \Magento\Ui\Component\Listing\Columns\Column
{
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource["data"]["items"])) {
            foreach ($dataSource["data"]["items"] as & $item) {
                $name = $this->getData("name");
                $id = $item["id"] ?? "X";
                $item[$name]["view"] = [
                    "href"=>$this->getContext()->getUrl(
                        "studiomoderna_content/article/edit",["id"=>$id]),
                    "label"=>__("Edit")
                ];
            }
        }

        return $dataSource;
    }    
    
}
