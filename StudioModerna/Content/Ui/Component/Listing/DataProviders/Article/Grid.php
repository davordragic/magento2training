<?php
namespace StudioModerna\Content\Ui\Component\Listing\DataProviders\Article;

use Magento\Ui\DataProvider\AbstractDataProvider;
use StudioModerna\Content\Model\ResourceModel\Article\CollectionFactory;

class Grid extends AbstractDataProvider
{    
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $collectionFactory->create();
    }
}
